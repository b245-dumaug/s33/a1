fetch('https://jsonplaceholder.typicode.com/todos')
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
  });

fetch('https://jsonplaceholder.typicode.com/todos')
  .then((response) => response.json())
  .then((data) => {
    let titles = data.map((item) => item.title);
    console.log(titles);
  });

fetch('https://jsonplaceholder.typicode.com/todos/1', { method: 'GET' })
  .then((response) => response.json())
  .then((result) => console.log(result));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
  },
})
  .then((response) => response.json())
  .then((data) => {
    console.log(`The item "${data.title}"  has a status of ${data.completed}`);
  });

fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    title: 'Created To Do List Item',
    completed: false,
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((result) => console.log(result));

fetch('https://jsonplaceholder.typicode.com/todos/5', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    title: 'updated to do list item',
    description: 'Hellow again',
    status: 'Pending',
    dateComplete: 'Pending',
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((result) => console.log(result));

fetch('https://jsonplaceholder.typicode.com/todos/5', {
  method: 'PATCH',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    title: 'delectus aut autem',
    description: 'Hellow again',
    status: 'Completed',
    dateComplete: `07/09/21`,
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((result) => console.log(result));

fetch('https://jsonplaceholder.typicode.com/todos/2', { method: 'DELETE' })
  .then((response) => response.json())
  .then((result) => console.log(result));
